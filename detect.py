# USAGE
# python detect.py --images images
# python detect.py --picamera 1
# python detect.py --fullscreen 1

import argparse

import cv2
import imutils
import numpy as np
import time
from imutils import paths
from imutils.object_detection import non_max_suppression

from imutils.video.videostream import VideoStream

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--picamera", type=int, default=-1,
                help="whether or not the Raspberry Pi camera should be used")
ap.add_argument("-fs", "--fullscreen", type=int, default=-1,
                help="whether or not use a Full Screen mode")
ap.add_argument("-i", "--images", required=False, help="path to images directory")
args = vars(ap.parse_args())

images = args["images"]
print("Images:", images)

# initialize the HOG descriptor/person detector
hog = cv2.HOGDescriptor()
# hog = cv2.HOGDescriptor((48,96), (16,16), (8,8), (8,8), 9)
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())


def handle_video():
    print("Handle Video input")
    # initialize the video stream and allow the camera sensor to warm up
    vs = VideoStream(usePiCamera=args["picamera"] > 0).start()
    time.sleep(2.0)

    frame_name = "Pedestrians"
    start_time = time.time()

    # loop over the frames from the video stream
    while True:
        current_time = time.time()
        if current_time - start_time < (1 / 24):
            continue
        start_time = current_time

        # grab the frame from the threaded video stream and resize it
        # to have a maximum width of 640 pixels
        frame = vs.read()
        frame = imutils.resize(frame, width=320)

        start = time.time()

        # detect people in the image
        rects, weights = hog.detectMultiScale(
            frame, winStride=(4, 4), padding=(8, 8), scale=1.05
        )

        # draw the original bounding boxes
        # for (x, y, w, h) in rects:
        #     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)

        # apply non-maxima suppression to the bounding boxes using a
        # fairly large overlap threshold to try to maintain overlapping
        # boxes that are still people
        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        # draw the final bounding boxes
        for (xA, yA, xB, yB) in pick:
            cv2.rectangle(frame, (xA, yA), (xB, yB), (0, 255, 0), 2)

        print("Time to process:", (time.time() - start))

        # make frame full screen
        if args["fullscreen"] > 0:
            cv2.namedWindow(frame_name, cv2.WND_PROP_FULLSCREEN)
            cv2.setWindowProperty(frame_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            # show the frame
        cv2.imshow(frame_name, frame)

        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break


def handle_images():
    print("Handle Images input")
    # loop over the image paths
    image_paths = list(paths.list_images(args["images"]))

    for image_path in image_paths:
        # load the image and resize it to (1) reduce detection time
        # and (2) improve detection accuracy
        image = cv2.imread(image_path)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        if image.shape[1] > image.shape[0]:
            image = imutils.resize(image, width=min(320, image.shape[1]))
        else:
            image = imutils.resize(image, height=min(240, image.shape[0]))
        # orig = image.copy()

        start = time.time()

        # detect people in the image
        rects, weights = hog.detectMultiScale(
            image, winStride=(4, 4), padding=(8, 8), scale=1.05
        )

        # draw the original bounding boxes
        # for (x, y, w, h) in rects:
        #     cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)

        # apply non-maxima suppression to the bounding boxes using a
        # fairly large overlap threshold to try to maintain overlapping
        # boxes that are still people
        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        # draw the final bounding boxes
        for (xA, yA, xB, yB) in pick:
            cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)

        print("Time to process:", (time.time() - start))

        # show some information on the number of bounding boxes
        # filename = image_path[image_path.rfind("/") + 1:]
        # print("[INFO] {}: {} original boxes, {} after suppression".format(
        #     filename, len(rects), len(pick)))

        # show the output images
        # cv2.imshow("Before NMS", orig)
        cv2.imshow("After NMS", image)
        cv2.waitKey(0)


if images is None:
    handle_video()
else:
    handle_images()

cv2.destroyAllWindows()
